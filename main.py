import smtplib
import speech_recognition as sr
import pyttsx3
from email.message import EmailMessage
import webbrowser
import time

chromepath = "C:\\Program Files\\Google\\Chrome Dev\\Application\\chrome.exe"
listener = sr.Recognizer()
engine = pyttsx3.init()


def talk(text):
    engine.say(text)
    engine.runAndWait()

def get_info():
    try:
        with sr.Microphone() as source:
            print('listening...')
            talk('listening...')
            voice = listener.listen(source)
            info = listener.recognize_google(voice)
            print(info)
            return info.lower()
    except:
        pass

def send_email(receiver, subject, message):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    # Make sure to give app access in your Google account
    server.login('your.gmail@gmail.com', 'your-password!')
    email = EmailMessage()
    email['From'] = 'Sender_Email'
    email['To'] = receiver
    email['Subject'] = subject
    email.set_content(message)
    server.send_message(email)


favourite = {
    'fav1': 'gmail1@gmail.com',
    'fav2': 'gmail2@gmail.com',
    'fav3': 'gmail3@gmail.com',
    'fav4': 'gmail4@gmail.com',
    'fav5': 'gmail5@gmail.com'
}

def get_email_info():
    talk('To Whom you want to send email')
    # name = get_info()
    talk("Enter the gmail id to send email: ")
    receiver = input("Enter the gmail id to send email: ")
    print(receiver)
    talk('What is the subject of your email?')
    subject = get_info()
    talk('Tell me the text in your email')
    message = get_info()
    send_email(receiver, subject, message)
    talk(f'Your email has been sent to {receiver}')
    print(f'Your email has been sent to {receiver}')
    talk("Let me show you the mail sir")
    print("Let me show you the mail sir")
    time.sleep(1)
    webbrowser.register('chrome',
    None,webbrowser.BackgroundBrowser(chromepath))
    webbrowser.get('chrome').open(f"https://mail.google.com/mail/u/0/?tab=rm&ogbl#sent") 

    talk('Do you want to send more email?')
    send_more = get_info()
    if 'yes' or 'yeah' in send_more:
        get_email_info()

    elif 'not' or 'no' in send_more:
        talk("Ok sir")

get_email_info()